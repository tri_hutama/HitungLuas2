package com.tri.app.hitungluas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText edtPanjang, edtLebar;
    private Button btnHitung;
    private TextView txtLuas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Hitung Luas Persegi Panjang");

        edtPanjang = (EditText)findViewById(R.id.edt_panjang);
        edtLebar = (EditText)findViewById(R.id.edt_lebar);
        btnHitung = (Button)findViewById(R.id.btn_hitung);
        txtLuas = (TextView)findViewById(R.id.txt_luas);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double panjang = 0;
                double lebar = 0;
                try{
                     panjang = Double.valueOf(edtPanjang.getText().toString());
                     lebar = Double.valueOf(edtLebar.getText().toString());

                }catch (NumberFormatException e){
                    Toast.makeText(MainActivity.this, "Nilai tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
                double luas = panjang * lebar;

                txtLuas.setText("Luas : "+luas);


            }
        });
    }
}